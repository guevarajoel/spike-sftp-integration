const Client = require('ssh2-sftp-client');
const fs = require('fs');

async function uploadFile(sourceFile) {
  const { SFTP_HOST: host, SFTP_USER: username, SFTP_PORT: port } = process.env;
  const remoteFile = `./${Date.now()}.json`;

  // Retrieve the private key used to authenticate to the SFTP server
  const privateKey = fs.readFileSync('keys/spike_sftp');

  console.log(
    'Uploading file to SFTP server',
    host,
    port,
    username,
    remoteFile
  );

  // Upload file to the SFTP server
  const sftpClient = new Client();

  await sftpClient
    .connect({
      host,
      port,
      username,
      privateKey,
    })
    .then(() => {
      console.log('Connected to SFTP server', host);
      return sftpClient.put(sourceFile, remoteFile);
    })
    .then(() => {
      sftpClient.end();
      console.log('Upload complete');
    })
    .catch((err) => {
      console.log('Error in uploading file to SFTP server', err);
    });
}

module.exports = {
  uploadFile,
};
