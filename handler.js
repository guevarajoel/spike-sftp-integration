const { S3 } = require('@aws-sdk/client-s3');
const { uploadFile } = require('./sftp-util');

const s3Client = new S3({});

async function processFile(event) {
  console.log('Received event', event);

  // Retrieve the first record
  const [record] = event.Records;

  // Sanity check that the record is for a newly created object
  if (!record || record.eventName !== 'ObjectCreated:Put') {
    console.warn('Record is not a new object');
    return {
      statusCode: 204,
    };
  }

  // Retrieve the bucket and object key names
  const bucket = record.s3.bucket.name;
  const key = decodeURIComponent(record.s3.object.key.replace(/\+/g, ' '));

  // Retrieve the S3 object
  const params = {
    Bucket: bucket,
    Key: key,
  };

  console.log('Retrieving S3 object with params', params);

  const file = await s3Client.getObject(params);

  // Upload the file to the SFTP server
  await uploadFile(file.Body);

  return {
    statusCode: 200,
    body: JSON.stringify({ message: 'success' }),
  };
}

module.exports = {
  processFile,
};
